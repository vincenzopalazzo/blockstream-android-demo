package io.vincenzopalazzo.jesplorademo.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;

public class StatusTx {

    private boolean confirmed;
    @SerializedName("block_height")
    private Integer blockHeight;
    @SerializedName("block_hash")
    private String blockHash;
    @SerializedName("block_time")
    private BigInteger blockTime;

    public boolean isConfirmed() {
        return confirmed;
    }

    public Integer getBlockHeight() {
        return blockHeight;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public BigInteger getBlockTime() {
        return blockTime;
    }
}
