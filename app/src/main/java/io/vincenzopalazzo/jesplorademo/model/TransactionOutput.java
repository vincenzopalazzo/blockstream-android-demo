package io.vincenzopalazzo.jesplorademo.model;



public class TransactionOutput {

    private String txid;
    private Integer vout;
    private StatusTx status;
    private Integer value;
    //Miss propriety

    public String getTxid() {
        return txid;
    }

    public Integer getVout() {
        return vout;
    }

    public StatusTx getStatus() {
        return status;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionOutput that = (TransactionOutput) o;
        return txid.equals(that.txid);
    }

    @Override
    public int hashCode() {
        return txid.hashCode();
    }
}
