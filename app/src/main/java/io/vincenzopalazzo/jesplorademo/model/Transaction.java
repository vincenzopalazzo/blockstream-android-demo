package io.vincenzopalazzo.jesplorademo.model;

import java.math.BigInteger;

public class Transaction{

    private String txid;
    private int version;
    private BigInteger locktime;
    private BigInteger size;
    private BigInteger weight;
    private BigInteger fee;
    private StatusTx status;
    //TODO added miss propriety


    public String getTxid() {
        return txid;
    }

    public int getVersion() {
        return version;
    }

    public BigInteger getLocktime() {
        return locktime;
    }

    public BigInteger getSize() {
        return size;
    }

    public BigInteger getWeight() {
        return weight;
    }

    public BigInteger getFee() {
        return fee;
    }

    public StatusTx getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return txid.equals(that.txid);
    }

    @Override
    public int hashCode() {
        return txid.hashCode();
    }
}
