package io.vincenzopalazzo.jesplorademo.util;

import java.lang.reflect.Type;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class HttpRequestFactory {

    private static HttpRequestFactory SINGLETON;

    public static HttpRequestFactory getInstance(){
        if(SINGLETON == null){
            SINGLETON = new HttpRequestFactory();
        }
        return SINGLETON;
    }

    private OkHttpClient client;
    private JSONConverter converter;

    private HttpRequestFactory(){
        client = new OkHttpClient();
        converter = new JSONConverter();
    }

    public Request buildGetRequest(String withUrl){
        if(withUrl == null || withUrl.isEmpty()){
            throw new IllegalArgumentException("Url null or empty");
        }
        return new Request.Builder().url(withUrl).build();
    }

    public <T> T execRequest(Request request, Type responseType){
        if(request == null){
            throw new IllegalArgumentException("Request null");
        }
        try{
            Response response = client.newCall(request).execute();
            String responseString = Objects.requireNonNull(response.body()).string();
            return converter.deserialize(responseString, responseType);
        }catch (Exception ex){
            Timber.e(ex.getCause());
            throw new RuntimeException(ex.getCause());
        }
    }

}
