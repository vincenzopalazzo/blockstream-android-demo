package io.vincenzopalazzo.jesplorademo.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

import timber.log.Timber;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class JSONConverter {

    private static final String patternFormat = "dd-MM-yyyy HH:mm:ss";

    private GsonBuilder gsonBuilder;

    public JSONConverter() {
        this.gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.setDateFormat(patternFormat);
    }

    public String serialize(Object object){
        Gson gson = gsonBuilder.create();
        return gson.toJson(object);
    }

    public <T> T deserialize(String fromString, Type responseType) {
        if (fromString == null || fromString.isEmpty()) {
            throw new IllegalArgumentException("String parameter null or empty");
        }
        try {
            Timber.d("From json string %s", fromString);
            Gson gson = gsonBuilder.create();
            return gson.fromJson(fromString, responseType);
        } catch (Exception ex) {
            Timber.e(ex.getCause());
            throw new RuntimeException(ex.getCause());
        }
    }

}
