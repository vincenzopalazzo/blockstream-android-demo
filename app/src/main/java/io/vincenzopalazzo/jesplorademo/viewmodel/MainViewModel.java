package io.vincenzopalazzo.jesplorademo.viewmodel;

import android.os.AsyncTask;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.vincenzopalazzo.jesplorademo.App;
import io.vincenzopalazzo.jesplorademo.model.Transaction;
import io.vincenzopalazzo.jesplorademo.model.TransactionOutput;
import io.vincenzopalazzo.jesplorademo.util.HttpRequestFactory;
import okhttp3.Request;
import timber.log.Timber;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class MainViewModel extends ViewModel {

    private List<String> addresses;
    private MutableLiveData<Set<TransactionOutput>> utxos;
    private MutableLiveData<List<Transaction>> totTx; //TODO change this orrible name

    public MainViewModel() {
        this.addresses = new ArrayList<>();
        this.totTx = new MutableLiveData<>(new ArrayList<>());
        this.utxos = new MutableLiveData<>(new HashSet<>());
        initListAddress();
    }

    //TODO(This could be fetch from other system such as DB or API REST to make
    // the UI reactive at change of this API)
    private void initListAddress() {
        addresses.add("tb1qtzrhlwxqcsufs8hvg4c3w33utf9hat4x9xlrf7");
        addresses.add("tb1q9q8mmlwuxy75utn6v0dr72n3s5pec436dzkchu");
        addresses.add("tb1q5yuzy28r9ca75t7nle34mecummx3ghh2huxu6u");
        addresses.add("tb1q96qev5k0ps9xhxl27n43l47q0hk4g98g02qvgf");
        addresses.add("tb1qkuv67xamatj303ugvx6u7x2jl2cmn2kguxrwep");
        addresses.add("tb1q0gll6nh3fs2vclk72n83htm89vxmvgnekcq80q");
        addresses.add("tb1qn4kz6xessgrsresex8vlmn2mehkwgk6x6ttt4e");
        addresses.add("tb1q7nhqtca98nezzce8wluyuaxez9w4lnmgrfjvwc");
        addresses.add("tb1queu8gdf2y0870de28fdj0q7gt3lszppp06p0z6");
        addresses.add("tb1quwf0s47w48pdlhs24m50n0m4h70ll2melgezcz");
        Timber.d("Init %d addresses", addresses.size());
    }

    public Integer getAmount() {
        int value = 0;
        for (TransactionOutput utxo : utxos.getValue()) {
            value += utxo.getValue();
        }
        return value;
    }

    public MutableLiveData<Set<TransactionOutput>> getUtxos() {
        return utxos;
    }

    public MutableLiveData<List<Transaction>> getTotTx() {
        return totTx;
    }

    public Transaction getTransactionAt(int position){
        if(position < 0){
            throw new IllegalArgumentException("position wrong");
        }
        return totTx.getValue().get(position);
    }

    //TODO change exception inside HttpRequestFactory an manage the http errors
    public void doRequest() {
        for (String address : addresses) {
            new BalanceForEachAddresses().execute(address);
            new ListOnChainTxByAddress().execute(address);
            new ListOnChainTxByAddress("mempool").execute(address);
        }
    }

    //This receive an address and return an amount
    private class BalanceForEachAddresses extends AsyncTask<String, Void, Set<TransactionOutput>> {

        @Override
        protected Set<TransactionOutput> doInBackground(String... strings) {
            String address = strings[0];
            Timber.d("Address to fetch utxo %s", address);

            String utxoByAddressuURL = String.format("%s/address/%s/utxo", App.BASE_API_URL, address);
            Timber.d("FINAL URL: %s", utxoByAddressuURL);

            Request request = HttpRequestFactory.getInstance().buildGetRequest(utxoByAddressuURL);

            List<TransactionOutput> utxos = HttpRequestFactory.getInstance()
                    .execRequest(request, new TypeToken<List<TransactionOutput>>() {
                    }.getType());


            Timber.d("Final result in doInBackground: %d", utxos.size());
            return new HashSet<>(utxos); //TODO This for big address should be wrong maybe is correct use BigInteger? !maybe yes
        }

        @Override
        protected void onPostExecute(Set<TransactionOutput> result) {
            super.onPostExecute(result);
            Timber.d("size utxo %s", result.size());
            Set join = utxos.getValue();
            join.addAll(result);
            utxos.setValue(join);
        }
    }

    //Get an address and return a list of transaction in memepool and onchain
    private class ListOnChainTxByAddress extends AsyncTask<String, Void, List<Transaction>> {

        private String where = "chain";

        public ListOnChainTxByAddress() { }

        public ListOnChainTxByAddress(String where) {
            this.where = where;
        }


        @Override
        protected List<Transaction> doInBackground(String... strings) {

            //TODO make a check before to access in strings array.
            String onChainAddr = String.format("%s/address/%s/txs/%s", App.BASE_API_URL, strings[0], where);
            Timber.d("FINAL URL: %s", onChainAddr);
            Request request = HttpRequestFactory.getInstance().buildGetRequest(onChainAddr);
            List<Transaction> tx = HttpRequestFactory.getInstance()
                    .execRequest(request, new TypeToken<List<Transaction>>() {
                    }.getType());

            Timber.d("List dimension in doInBackground %d", tx.size());
            return tx;
        }

        @Override
        protected void onPostExecute(List<Transaction> transactions) {
            super.onPostExecute(transactions);
            //TODO I can do this operation more compact
            List joinTx = totTx.getValue();
            Timber.d("Dimension tx before join %d", joinTx.size());
            joinTx.addAll(transactions);
            Timber.d("Dimension tx after join %d", joinTx.size());
            //I can improve this filter
            Set<Transaction> removeDuplicate = new HashSet<>(joinTx);
            totTx.setValue(new ArrayList<>(removeDuplicate));
        }
    }


}
