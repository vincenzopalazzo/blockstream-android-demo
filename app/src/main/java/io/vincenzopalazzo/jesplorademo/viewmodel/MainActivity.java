package io.vincenzopalazzo.jesplorademo.viewmodel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Set;
import java.util.Timer;

import io.vincenzopalazzo.jesplorademo.R;
import io.vincenzopalazzo.jesplorademo.databinding.ActivityMainBinding;
import io.vincenzopalazzo.jesplorademo.databinding.TxItemViewBinding;
import io.vincenzopalazzo.jesplorademo.model.Transaction;
import io.vincenzopalazzo.jesplorademo.model.TransactionOutput;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class MainActivity extends AppCompatActivity {

    private MainViewModel viewModel;
    private ActivityMainBinding binding;
    private ListTransactionAdapter adapter;
    private Handler fetchAPI;
    private Runnable timerAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(this.binding.getRoot());

        this.viewModel = new ViewModelProvider(this).get(MainViewModel.class);

        //Amount observer binding
        this.viewModel.getUtxos().observe(this, new Observer<Set<TransactionOutput>>() {
            @Override
            public void onChanged(Set<TransactionOutput> value) {
                int amount = viewModel.getAmount();
                binding.textViewAmount.setText(amount + " BTC"); //TODO make the valute dinamic
            }
        });

        this.viewModel.getTotTx().observe(this, new Observer<List<Transaction>>() {
            @Override
            public void onChanged(List<Transaction> transactions) {
                adapter.notifyDataSetChanged();
            }
        });

        this.viewModel.doRequest();

        this.binding.downloadFabButton.setOnClickListener(v -> viewModel.doRequest());

        this.binding.listTransaction.setLayoutManager(new LinearLayoutManager(this));
        this.adapter = new ListTransactionAdapter(viewModel);
        this.binding.listTransaction.setAdapter(adapter);

        fetchAPI = new android.os.Handler();

        timerAction = () -> {
            showMessageOnSnackBar("Update timer");
            viewModel.doRequest();
        };

        showMessageOnSnackBar("Loading data :-)");
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.startHandler();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.stopHandler();
    }

    //TODO improve this action
    //If I need to update also when the app is closed I can run a Service
    void startHandler() {
        fetchAPI.postDelayed(timerAction, 5 * 10000);
    }

    void stopHandler() {
        fetchAPI.removeCallbacks(timerAction);
    }

    private void showMessageOnSnackBar(String message, int duration){
        Snackbar.make(this.binding.getRoot(), message, duration).show();
    }

    private void showMessageOnSnackBar(String message){
        this.showMessageOnSnackBar(message, Snackbar.LENGTH_LONG);
    }

    protected static class ListTransactionAdapter extends RecyclerView.Adapter<ListTransactionAdapter.Holder> {

        private MainViewModel viewModel;
        private TxItemViewBinding itemViewBinding;

        public ListTransactionAdapter(MainViewModel viewModel) {
            this.viewModel = viewModel;
        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            this.itemViewBinding = TxItemViewBinding.inflate(LayoutInflater.from(parent.getContext()));
            View root = itemViewBinding.getRoot();
            return new Holder(root);
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            Transaction transaction = viewModel.getTransactionAt(position);
            itemViewBinding.hashTextView.setText(transaction.getTxid());
            String stringStatus = transaction.getStatus().isConfirmed() ? "Confirmed" : "Pending";
            itemViewBinding.status.setText(stringStatus);
        }

        @Override
        public int getItemCount() {
            if(viewModel == null || viewModel.getTotTx().getValue() == null){
                return 0;
            }
            return viewModel.getTotTx().getValue().size();
        }

        public static class Holder extends RecyclerView.ViewHolder {
            public Holder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }


}