package io.vincenzopalazzo.jesplorademo;

import android.app.Application;

import timber.log.Timber;

/**
 * @author https://github.com/vincenzopalazzo
 */
public class App extends Application {

    public static final String BASE_API_URL = "https://blockstream.info/testnet/api";

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}
